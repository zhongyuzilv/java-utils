package com.gitee.zhongyuzilv.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author 赵滨
 * @name PublicAttribute
 * @date 2021-05-21 16:11
 * @descriptions 公共属性
 */
@Getter
@AllArgsConstructor
public enum PublicAttributeEnum {
    /**
     * 表中的公共属性，在代码中的转换名
     */
    SEQ_ID("seqId"),
    DELETE_FLAG("deleteFlag"),
    CREATE_TIME("createTime"),
    CREATE_USER("createUser"),
    UPDATE_TIME("updateTime"),
    UPDATE_USER("updateUser"),
    ;
    private final String attrName;
}
