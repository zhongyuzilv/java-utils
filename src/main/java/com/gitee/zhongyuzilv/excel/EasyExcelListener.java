package com.gitee.zhongyuzilv.excel;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author 赵滨
 * @name EasyExcelListener
 * @date 2021-06-22 09:11
 * @descriptions
 */
public class EasyExcelListener<T, R> extends AnalysisEventListener<T> {

    /**
     * 最大条数
     */
    public static final int MAX_COUNT = -1;

    /**
     * 批量条数
     */
    private final int batchCount;

    /**
     * 数据集合
     */
    List<T> list = new ArrayList<>();

    /**
     * 保存方法
     */
    private final Function<List<T>, R> function;

    /**
     * 结果
     */
    private R result;

    /**
     * 注入
     */
    public EasyExcelListener(int batchCount, Function<List<T>, R> function) {
        this.batchCount = batchCount;
        this.function = function;
    }

    /**
     * 每行读取
     */
    @Override
    public void invoke(T t, AnalysisContext analysisContext) {
        list.add(t);
        if (list.size() >= batchCount && batchCount != MAX_COUNT) {
            saveData();
        }
    }

    /**
     * 最终执行
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        saveData();
    }

    /**
     * 存储数据
     */
    private void saveData() {
        result = function.apply(list);
        list.clear();
    }

    /**
     * 获取结果
     */
    public R getResult() {
        return result;
    }
}
