package com.gitee.zhongyuzilv.excel;

import com.alibaba.excel.EasyExcel;
import lombok.SneakyThrows;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author 赵滨
 * @name EasyExcelUtils
 * @date 2021-06-21 15:34
 * @descriptions
 */
public class EasyExcelUtils {

    public static final String DEFAULT_SHEET_NAME = "Sheet";

    /**
     * 下载 - 根据list数据，list不能为空
     */
    public static <T> void download(HttpServletResponse response, String name, List<T> data) {
        if (data.size() == 0) {
            throw new RuntimeException("没有可以生成的Excel数据！");
        }
        download(response, name, DEFAULT_SHEET_NAME, data, (Class<T>) data.get(0).getClass());
    }

    /**
     * 下载 - 根据clazz，没有的context
     */
    public static <T> void download(HttpServletResponse response, String name, Class<T> clazz) {
        List<T> data = new ArrayList<>();
        download(response, name, DEFAULT_SHEET_NAME, data, clazz);
    }

    @SneakyThrows
    private static <T> void download(HttpServletResponse response, String fileName, String sheetName, List<T> data, Class<T> clazz) {
        response.setContentType("application/vnd.ms-excel");
        response.setCharacterEncoding("utf-8");
        fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
        EasyExcel.write(response.getOutputStream(), clazz).sheet(sheetName).doWrite(data);
    }

    /**
     * 上传 - 默认一次性存储
     */
    public static <T, R> R upload(MultipartFile file, Class<T> clazz, Function<List<T>, R> function) {
        return upload(file, clazz, function, EasyExcelListener.MAX_COUNT);
    }

    @SneakyThrows
    private static <T, R> R upload(MultipartFile file, Class<T> clazz, Function<List<T>, R> function, int batchCount) {
        EasyExcelListener<T, R> listener = new EasyExcelListener<>(batchCount, function);
        EasyExcel.read(file.getInputStream(), clazz, listener).sheet(DEFAULT_SHEET_NAME).doRead();
        return listener.getResult();
    }

}
