package com.gitee.zhongyuzilv.bean;

import com.gitee.zhongyuzilv.enums.PublicAttributeEnum;

import java.util.Date;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * @author 赵滨
 * @name BeanUtils
 * @date 2021-06-16 15:55
 * @descriptions
 */
public class BeanUtils {

    public static void initSave(Object o) {

        ReflectUtils.setField(o, PublicAttributeEnum.DELETE_FLAG, 0);
        ReflectUtils.setField(o, PublicAttributeEnum.CREATE_USER, "");
        ReflectUtils.setField(o, PublicAttributeEnum.CREATE_TIME, new Date());
        ReflectUtils.setField(o, PublicAttributeEnum.UPDATE_USER, "");
        ReflectUtils.setField(o, PublicAttributeEnum.UPDATE_TIME, new Date(0));
    }

    public static <T, R> void replaceNull(T t, Function<T, R> get, BiConsumer<T, R> set, R replace) {
        if (get.apply(t) == null) {
            set.accept(t, replace);
        }
    }

    public static <T> void replaceNullString(T t, Function<T, String> get, BiConsumer<T, String> set) {
        if (get.apply(t) == null) {
            set.accept(t, "");
        }
    }

    public static <T> void replaceNullInteger(T t, Function<T, Integer> get, BiConsumer<T, Integer> set) {
        if (get.apply(t) == null) {
            set.accept(t, 0);
        }
    }

    public static <T> void replaceNullDouble(T t, Function<T, Double> get, BiConsumer<T, Double> set) {
        if (get.apply(t) == null) {
            set.accept(t, 0d);
        }
    }

    public static <T> void replaceNullDate(T t, Function<T, Date> get, BiConsumer<T, Date> set) {
        if (get.apply(t) == null) {
            set.accept(t, new Date(0));
        }
    }

}
