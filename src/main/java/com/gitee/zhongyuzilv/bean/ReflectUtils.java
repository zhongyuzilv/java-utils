package com.gitee.zhongyuzilv.bean;


import com.gitee.zhongyuzilv.enums.PublicAttributeEnum;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 赵滨
 * @name FieldUtils
 * @date 2021-05-21 12:02
 * @descriptions 反射工具包
 */
public class ReflectUtils {

    /**
     * 通过反射，获取指定类的含有注解类的字段集合
     * @param clazz 指定类
     * @param annotationClass 注解类
     * @return 字段集合
     */
    public static Field[] getAnnotationClassFields(Class<?> clazz, Class<? extends Annotation> annotationClass) {
        List<Field> list = new ArrayList<>();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(annotationClass)) {
                list.add(field);
            }
        }
        Field[] fields = new Field[list.size()];
        return list.toArray(fields);
    }

    /**
     * 通过反射，获取对象的字段值，并忽略其中的异常
     * @param object 取值对象
     * @param publicAttribute 字段名的公共属性
     * @param clazz 类型的类
     * @param <T> 类型
     * @return 字段值
     */
    public static <T> T getField(Object object, PublicAttributeEnum publicAttribute, Class<T> clazz) {
        try {
            Field field = object.getClass().getDeclaredField(publicAttribute.getAttrName());
            field.setAccessible(true);
            return (T) field.get(object);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 通过反射，给对象的字段赋值，并忽略其中的异常
     * @param object 赋值对象
     * @param publicAttribute 字段名的公共属性
     * @param value 值
     */
    public static void setField(Object object, PublicAttributeEnum publicAttribute, Object value) {
        try {
            Field field = object.getClass().getDeclaredField(publicAttribute.getAttrName());
            field.setAccessible(true);
            field.set(object, value);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * 指定类中是否存在指定的字段
     * @param clazz 指定类
     * @param publicAttribute 字段名的公共属性
     * @return true：存在；false：不存在
     */
    public static boolean hasField(Class<?> clazz, PublicAttributeEnum publicAttribute) {
        for (Field field : clazz.getDeclaredFields()) {
            if (publicAttribute.getAttrName().equals(field.getName())) {
                return true;
            }
        }
        return false;
    }
}
